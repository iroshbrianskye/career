$(function() {
    $("#fe").each(function() {
        var $this = $(this);
        //Add new entry
        $("#far").click(function() {
            var target = $($(this).data("target"))
            //console.log(target);
            var oldrow = target.find(".uno:last");
             // console.log(oldrow);
            var row = oldrow.clone(true, true);

            // console.log(row.find(":input")[0]);
            // console.log(row.find(":input:last")[0]);
            var elem_id = row.find(":input")[0].id;
            var elem_num = parseInt(elem_id.replace(/.*-(\d{1,4})-.*/m, '$1')) + 1;
            row.attr('data-id', elem_num);

            row.find(":input").each(function() {
                //console.log(this);
                 var id = $(this).attr('id').replace('-' + (elem_num - 1) + '-', '-' + (elem_num) + '-');
                 $(this).attr('name', id).attr('id', id).val('').removeAttr("checked");
            });
            oldrow.after(row);
        }); //End add new entry
                //Remove row
        $("button[data-toggle=fieldset-remove-row]").click(function() {
            // if($this.find("[data-toggle=fieldset-entry]").length > 1) {
                var thisRow = $(this).closest("[data-toggle=fieldset-entry]");
                thisRow.remove();
            // }
        }); //End remove row
    });

    $("div#fe2").each(function() {
        var $this = $(this);
        //Add new entry
        $("#far2").click(function() {
            var target = $($(this).data("target"))
            //console.log(target);
            var oldrow = target.find(".dos:last");
             // console.log(oldrow);
            var row = oldrow.clone(true, true);

            // console.log(row.find(":input")[0]);
            // console.log(row.find(":input:last")[0]);
            var elem_id = row.find(":input")[0].id;
            var elem_num = parseInt(elem_id.replace(/.*-(\d{1,4})-.*/m, '$1')) + 1;
            row.attr('data-id', elem_num);

            row.find(":input").each(function() {
                //console.log(this);
                var id = $(this).attr('id').replace('-' + (elem_num - 1) + '-', '-' + (elem_num) + '-');
                $(this).attr('name', id).attr('id', id).val('').removeAttr("checked");
            });
            oldrow.after(row);
        }); //End add new entry
        //Remove row
        $("button[data-toggle=fieldset-remove-row]").click(function() {
            // if($this.find("[data-toggle=fieldset-entry]").length > 1) {
                var thisRow = $(this).closest("[data-toggle=fieldset-entry]");
                thisRow.remove();
            // }
        }); //End remove row
    });
    $("div#fe3").each(function() {
        var $this = $(this);
        //Add new entry
        $("#far3").click(function() {
            var target = $($(this).data("target"))
            //console.log(target);
            var oldrow = target.find(".tre:last");
             // console.log(oldrow);
            var row = oldrow.clone(true, true);

            // console.log(row.find(":input")[0]);
            // console.log(row.find(":input:last")[0]);
            var elem_id = row.find(":input")[0].id;
            var elem_num = parseInt(elem_id.replace(/.*-(\d{1,4})-.*/m, '$1')) + 1;
            row.attr('data-id', elem_num);

            row.find(":input").each(function() {
                //console.log(this);
                var id = $(this).attr('id').replace('-' + (elem_num - 1) + '-', '-' + (elem_num) + '-');
                $(this).attr('name', id).attr('id', id).val('').removeAttr("checked");
            });
            oldrow.after(row);
        }); //End add new entry

        //Remove row
        $("button[data-toggle=fieldset-remove-row]").click(function() {
            // if($this.find("[data-toggle=fieldset-entry]").length > 1) {
                var thisRow = $(this).closest("[data-toggle=fieldset-entry]");
                thisRow.remove();
            // }
        }); //End remove row
    });
});
