from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import ValidationError
from wtforms import Form as NoCsrfForm
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextField,
    DateField,
    DecimalField,
    SelectField,
    TextAreaField,
    IntegerField,
    FileField,
    FieldList,
    FormField
)
from wtforms import RadioField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired, Required, Optional, Regexp
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES, TEXT
from flask_login import current_user
from app.models import User
from app.models import *

certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


def get_pk(obj):
    return str(obj)


class MessageForm(Form):
    message = TextAreaField(
        "Message", validators=[DataRequired(), Length(min=0, max=140)]
    )
    submit = SubmitField("Submit")


class ProfileForm(FlaskForm):
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address")
    dob = DateField("Date Of Birth", validators=[DataRequired()], format="%d/%m/%Y")

    submit = SubmitField("Submit")


class RequiredIf(object):

    def __init__(self, *args, **kwargs):
        self.conditions = kwargs

    def __call__(self, form, field):
        for name, data in self.conditions.items():
            if name not in form._fields:
                Optional(form, field)
            else:
                condition_field = form._fields.get(name)
                if condition_field.data == data and not field.data:
                    DataRequired()(form, field)
        Optional()(form, field)


class DataEntryForm(FlaskForm):
    job_reference_number = StringField("Job Ref Number")
    job_title = StringField("Job Title")
    date_entered = DateField("Date Entered", format="%d%m%Y", default=datetime.now())
    title = StringField("Title")
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    second_name = StringField(
        "Second name", validators=[InputRequired(), Length(1, 64)]
    )
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    id_number = StringField("ID Number")
    home_county = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )
    ethnicity = StringField("Ethnicity", validators=[InputRequired(), Length(1, 100)])
    age = StringField("Age")
    civil_status = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("cohabitation", "Cohabitation"),
            ("divorced", "Divorced"),
            ("domestic partner", "Domestic Partner"),
            ("legally separated", "Legally Separated"),
            ("married", "Married"),
            ("separated", "Separated"),
            ("single", "Single"),
            ("undetermined", "Undetermined"),
        ],
        default=''
    )
    nationality = StringField(
        "Nationality", validators=[InputRequired(), Length(1, 64)]
    )
    phone_number_personal = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_alternate = StringField("Alternate Number")
    disability = BooleanField(default=False)
    disability_nature = StringField("Disability Nature", validators=[RequiredIf(disability=True)])
    disability_registration_details = StringField("Disability Registration Details")
    highest_level_education = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("kcpe", "KCPE"),
            ("kcse", "KCSE"),
            ("diploma", "Diploma"),
            ("certificate", "Certificate"),
            ("degree", "Degree"),
            ("masters", "Masters"),
            ("doctorate", "Doctorate"),
        ],
        default=''
    )
    phd_certificate_obtained = StringField(
        "Certification Obtained"
    )
    masters_certificate_obtained = StringField(
        "Certification Obtained"
    )
    degree_certificate_obtained = StringField(
        "Certification Obtained"
    )
    diploma_certificate_obtained = StringField(
        "Certification Obtained"
    )
    higher_diploma_certificate_obtained = StringField(
        "Certification Obtained"
    )
    relevant_certification = StringField(
        "Name of Certification"
    )
    registration = StringField(
        "Registration To A Body"
    )
    total_work_experience = IntegerField(
        "Total work experience", validators=[DataRequired()]
    )
    relevant_work_experience = IntegerField(
        "Relevant work experience", validators=[DataRequired()]
    )
    mode_of_application = StringField(
        "Mode of application"
    )
    data_entered_by = IntegerField(
        "Entered by"
    )
    submit = SubmitField("Submit")
