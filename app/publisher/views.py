import uuid

from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    jsonify,
    flash,
    send_from_directory)
from flask_ckeditor import upload_success, upload_fail
from flask_login import current_user, login_required, login_user
import PIL, simplejson, traceback
from PIL import Image
from flask_rq import get_queue
from flask_uploads import patch_request_class

from app.models.job_listings import *
from app.models.applications import *
from app.models.education_documents import *
from app.models.users import *
from app.publisher.forms import *
from app.auth.forms import *
from app import db
from app.email import send_email
from app.models import User, Listing
from app.auth.email import send_password_reset_email
from app.auth.admin_decorators import check_confirmed
from app.decorators import checks_required
from werkzeug import secure_filename
from app.lib.upload_file import uploadfile
from app.auth.email import send_applicant_submitted

publisher = Blueprint("publisher", __name__)


@publisher.route("/")
@login_required
@check_confirmed
def dashboard():
    return render_template("publishers/dashboard.html")


@publisher.route("/applications")
@login_required
@check_confirmed
def applications():
    manual_applications = ManualEntry.query.all()
    return render_template("publishers/applications.html", manual_applications=manual_applications)


@publisher.route("/add_applicant", methods=["POST", "GET"])
@login_required
@check_confirmed
def add_applicant():
    form = DataEntryForm()
    if form.validate_on_submit():
        # new_age = abs(form.age.data - 2020)
        manual_entry = ManualEntry(
            job_reference_number=form.job_reference_number.data,
            job_title=form.job_title.data,
            date_entered=datetime.now(),
            first_name=form.first_name.data,
            surname=form.surname.data,
            second_name=form.second_name.data,
            gender=form.gender.data,
            id_number=form.id_number.data,
            email=form.email.data,
            phone_number_personal=form.phone_number_personal.data,
            address=form.address.data,
            age=form.age.data,
            home_county=form.home_county.data,
            ethnicity=form.ethnicity.data,
            phone_number_alternate=form.phone_number_alternate,
            title=form.title.data,
            civil_status=form.civil_status.data,
            nationality=form.nationality.data,
            disability=form.disability.data,
            disability_nature=form.disability_nature.data,
            disability_registration_details=form.disability_registration_details.data,
            highest_level_education=form.highest_level_education.data,
            phd_certificate_obtained=form.phd_certificate_obtained.data,
            masters_certificate_obtained=form.masters_certificate_obtained.data,
            degree_certificate_obtained=form.degree_certificate_obtained.data,
            diploma_certificate_obtained=form.diploma_certificate_obtained.data,
            higher_diploma_certificate_obtained=form.higher_diploma_certificate_obtained.data,
            relevant_certification=form.relevant_certification.data,
            registration=form.registration.data,
            total_work_experience=form.total_work_experience.data,
            relevant_work_experience=form.relevant_work_experience.data,
            mode_of_application=form.mode_of_application.data,
            data_entered_by=current_user.id
        )
        db.session.add(manual_entry)
        db.session.commit()
        flash("Manual Application Submitted Successfully.", "success")
        return redirect(url_for("publisher.applications"))
    return render_template("publishers/add_applicant.html", form=form)
