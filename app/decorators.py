from functools import wraps

from flask import abort, flash, redirect, url_for
from flask_login import current_user

from app.models import Permission


def permission_required(permission):
    """Restrict a view to users with the given permission."""

    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.can(permission):
                abort(403)
            return f(*args, **kwargs)

        return decorated_function

    return decorator


def checked_boxes_required():
    """Restrict a view to users who have completed the forms."""

    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            if not current_user.can_submit():
                flash("You have incomplete forms. Make sure the checkboxes are all marked green.", "danger")
                return redirect(url_for("applicant.dashboard"))
            return f(*args, **kwargs)

        return decorated_function

    return decorator


def admin_required(f):
    return permission_required(Permission.ADMINISTRATOR)(f)


def checks_required(f):
    return checked_boxes_required()(f)
