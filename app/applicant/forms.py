from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import ValidationError
from wtforms import Form as NoCsrfForm
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextField,
    DateField,
    DecimalField,
    SelectField,
    TextAreaField,
    IntegerField,
    FileField,
    FieldList,
    FormField
)
from wtforms import RadioField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired, Required, Optional, Regexp
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES, TEXT
from flask_login import current_user
from app.models import User
from app.models import *

certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


def get_pk(obj):
    return str(obj)


class ReviewForm(FlaskForm):
    stars = RadioField("Label", coerce=int)
    comment = TextAreaField("Comment", validators=[InputRequired(), Length(1, 300)])
    submit = SubmitField("Submit")


class MessageForm(Form):
    message = TextAreaField(
        "Message", validators=[DataRequired(), Length(min=0, max=140)]
    )
    submit = SubmitField("Submit")


class ProfileForm(FlaskForm):
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address")
    dob = DateField("Date Of Birth", validators=[DataRequired()], format="%d/%m/%Y")

    submit = SubmitField("Submit")


class RequiredIf(object):

    def __init__(self, *args, **kwargs):
        self.conditions = kwargs

    def __call__(self, form, field):
        for name, data in self.conditions.items():
            if name not in form._fields:
                Optional(form, field)
            else:
                condition_field = form._fields.get(name)
                if condition_field.data == data and not field.data:
                    DataRequired()(form, field)
        Optional()(form, field)


class PersonalDetailsForm(FlaskForm):
    title = StringField("Title")
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    second_name = StringField(
        "Second name", validators=[InputRequired(), Length(1, 64)]
    )
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    home_county = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )
    ethnicity = StringField("Ethnicity", validators=[InputRequired(), Length(1, 100)])
    dob = DateField("Year Of Birth", validators=[DataRequired()], format="%Y")

    civil_status = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("cohabitation", "Cohabitation"),
            ("divorced", "Divorced"),
            ("domestic partner", "Domestic Partner"),
            ("legally separated", "Legally Separated"),
            ("married", "Married"),
            ("separated", "Separated"),
            ("single", "Single"),
            ("undetermined", "Undetermined"),
        ],
        default=''
    )
    nationality = StringField(
        "Nationality", validators=[InputRequired(), Length(1, 64)]
    )
    disability = BooleanField(default=False)
    disability_nature = StringField("Disability Nature", validators=[RequiredIf(disability=True)])
    disability_registration_details = StringField("Disability Registration Details")

    submit = SubmitField("Submit")


class ContactInfoForm(FlaskForm):
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    county = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )
    phone_number_personal = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    phone_number_alternate = StringField("Alternate Number")
    submit = SubmitField("Save")


class FieldForm(NoCsrfForm):
    year_ended = StringField("Year Ended", validators=[InputRequired(), Length(1, 64)])
    name_of_institution = StringField(
        "Name of Institution", validators=[InputRequired(), Length(1, 64)]
    )
    certificate_obtained = StringField(
        "Certification Obtained", validators=[InputRequired(), Length(1, 64)]
    )


class KankaForm(NoCsrfForm):
    year_ended = StringField("Year Ended")
    name_of_certification = StringField(
        "Name of Certification"
    )


class BadoForm(NoCsrfForm):
    year_joined = StringField("Year Joined")
    name_of_membership = StringField(
        "Name of Membership"
    )


class EducationDetailsForm(FlaskForm):
    education = FieldList(FormField(FieldForm, default=lambda: Education()), min_entries=1)
    professional = FieldList(FormField(KankaForm, default=lambda: Professional()), min_entries=1)
    membership = FieldList(FormField(BadoForm, default=lambda: Membership()), min_entries=1)
    highest_level_education = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("kcpe", "KCPE"),
            ("kcse", "KCSE"),
            ("diploma", "Diploma"),
            ("certificate", "Certificate"),
            ("degree", "Degree"),
            ("masters", "Masters"),
            ("doctorate", "Doctorate"),
        ],
        default=''
    )
    exact_degree_diploma_title = StringField(
        "Exact Degree/Diploma/Certificate", validators=[InputRequired(), Length(1, 64)]
    )
    phd_year_from = StringField("Year From")
    phd_year_to = StringField("Year To")
    phd_name_of_institution = StringField(
        "Name of Institution"
    )
    phd_certificate_obtained = StringField(
        "Certification Obtained"
    )
    masters_year_from = StringField("Year Ended")
    masters_year_to = StringField("Year To")
    masters_name_of_institution = StringField(
        "Name of Institution"
    )
    masters_certificate_obtained = StringField(
        "Certification Obtained"
    )
    degree_year_from = StringField("Year Ended")
    degree_year_to = StringField("Year To")
    degree_name_of_institution = StringField(
        "Name of Institution"
    )
    degree_certificate_obtained = StringField(
        "Certification Obtained"
    )
    diploma_year_from = StringField("Year Ended")
    diploma_year_to = StringField("Year To")
    diploma_name_of_institution = StringField(
        "Name of Institution"
    )
    diploma_certificate_obtained = StringField(
        "Certification Obtained"
    )
    higher_diploma_year_from = StringField("Year Ended")
    higher_diploma_year_to = StringField("Year To")
    higher_diploma_name_of_institution = StringField(
        "Name of Institution"
    )
    higher_diploma_certificate_obtained = StringField(
        "Certification Obtained"
    )
    certificate_year_from = StringField("Year Ended")
    certificate_year_to = StringField("Year To")
    certificate_name_of_institution = StringField(
        "Name of Institution"
    )
    certificate_certificate_obtained = StringField(
        "Certification Obtained"
    )
    submit = SubmitField("Save")


class EdForm(FlaskForm):
    cv_url = StringField()
    cover_letter_url = StringField()
    application_form_url = StringField()


class ProfessionalForm(FlaskForm):
    professional = FieldList(FormField(KankaForm), min_entries=1)


class MembershipForm(FlaskForm):
    membership = FieldList(FormField(BadoForm), min_entries=1)


class Long(FlaskForm):
    det1 = FormField(EducationDetailsForm)
    det2 = FormField(ProfessionalForm)
    det3 = FormField(MembershipForm)
    det4 = FormField(EdForm)
    submit = SubmitField("Save")


class TreshForm(NoCsrfForm):
    year_from = IntegerField(
        "Year From", validators=[InputRequired()]
    )
    year_to = IntegerField("Year To", validators=[InputRequired()])
    name_of_institution = StringField(
        "Name of Institution", validators=[InputRequired()]
    )
    designation = StringField("designation", validators=[InputRequired()])
    gross_salary = StringField("Gross Salary", validators=[InputRequired()])


class EmploymentForm(FlaskForm):
    employment_history = FieldList(
        FormField(TreshForm, default=lambda: EmploymentHistory()), min_entries=1
    )
    submit = SubmitField("Save")


class EmpForm(FlaskForm):
    ministry = StringField("Ministry")
    station = StringField("Station")
    personal_employment_no = StringField("Personal/Employment Number")
    present_substantive_post = StringField("Present Substantive Post")
    job_group = StringField("Job Group")
    date_of_current_appointment = DateField("Date of Current Appointment", format="%d/%m/%Y")
    terms_of_service = SelectField(
        choices=[("", ""), ("permanent & pensionable", "PERMANENT & PENSIONABLE"),
                 ("contract", "CONTRACT"), ("other", "OTHER")], default=""
    )
    if_other = TextAreaField("If Other, specify")
    gross_salary = StringField("Gross Salary")
    current_employer = StringField("Current Employer")
    effective_date = DateField("Effective Date", format="%d/%m/%Y")
    position_held = StringField("Position Held")
    submit = SubmitField("Save")


class FamilyForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    relationship = StringField(
        "Relationship", validators=[InputRequired(), Length(1, 64)]
    )
    phone_number = StringField(
        "Phone Number", validators=[InputRequired(), Length(1, 64)]
    )
    dob = DateField("Date Of Birth", validators=[DataRequired()], format="%d/%m/%Y")

    submit = SubmitField("Save")


class ElForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address = StringField("Email", validators=[InputRequired(), Length(1, 64)])
    occupation = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    name_2 = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address_2 = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_2 = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address_2 = StringField("Email", validators=[InputRequired(), Length(1, 64)])
    occupation_2 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution_2 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    name_3 = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address_3 = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_3 = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address_3 = StringField("Email", validators=[InputRequired(), Length(1, 64)])
    occupation_3 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution_3 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    submit = SubmitField("Save")


class SelectJob(FlaskForm):
    job = QuerySelectField(
        "Select Job",
        validators=[InputRequired()],
        get_label="job_title",
        query_factory=lambda: db.session.query(Listing).order_by("job_title").filter_by(published=True),
        get_pk=get_pk,
    )
    user_id = IntegerField("User ID")
    submit = SubmitField("Select Job")


class StartAppForm(FlaskForm):
    submit = SubmitField("APPLY JOB")


class EditStartAppForm(FlaskForm):
    submit = SubmitField("EDIT JOB APPLICATION")


class ApplicationForm(FlaskForm):
    job_listing_id = IntegerField("Job ID")
    user_id = IntegerField("User ID")
    complete_profile = StringField("Complete Profile")

    submit = SubmitField("APPLY")


def validate_file_extension(form, field):
    import os
    ext = os.path.splitext(field.data)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension. PDF Documents Only!')


class MyBaseForm(FlaskForm):
    class Meta:
        def bind_field(self, form, unbound_field, options):
            filters = unbound_field.kwargs.get('filters', [])
            filters.append(my_strip_filter)
            return unbound_field.bind(form=form, filters=filters, **options)


def my_strip_filter(value):
    if value is not None and hasattr(value, 'strip'):
        value = value.replace("  ", " ").replace(" ", "_").replace("(", "").replace(")", "")
        return value
    return value


class ApplicationDocuments(MyBaseForm):
    cv_url = StringField(validators=[DataRequired(), validate_file_extension])
    cover_letter_url = StringField(validators=[DataRequired(), validate_file_extension])
    application_form_url = StringField(validators=[DataRequired(), validate_file_extension])
    terms_and_conditions = BooleanField(validators=[DataRequired()], default=False)
    application_id = IntegerField()
    years_of_experience = StringField(validators=[DataRequired()])
    license = StringField()

    submit = SubmitField("SUBMIT MY APPLICATION TO THE ABOVE VACANCY")


class EditApplicationDocuments(MyBaseForm):
    cv_url = StringField(validators=[DataRequired(), validate_file_extension])
    cover_letter_url = StringField(validators=[DataRequired(), validate_file_extension])
    application_form_url = StringField(validators=[DataRequired(), validate_file_extension])
    terms_and_conditions = BooleanField(validators=[DataRequired()], default=False)
    application_id = IntegerField()
    years_of_experience = IntegerField(validators=[DataRequired()])
    license = StringField()

    submit = SubmitField("EDIT")

