from datetime import datetime

from .. import db


class Professional(db.Model):
    __tablename__ = 'professional'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    year_ended = db.Column(db.String(64), index=True)
    name_of_certification = db.Column(db.String(64), index=True)

    def __repr__(self):
        return '<Profession\'%s\'>' % self.name_of_certification
