from datetime import datetime
from app.models.applications import Application
from sqlalchemy import or_, and_
from flask import current_app
from .. import db
from sqlalchemy.ext.hybrid import hybrid_method

listing_tags = db.Table('listing_tags',
                        db.Column('job_listing_id', db.Integer, db.ForeignKey('job_listings.id')),
                        db.Column('category_id', db.Integer, db.ForeignKey('categories.id')))


class Listing(db.Model):
    __tablename__ = 'job_listings'
    id = db.Column(db.Integer, primary_key=True)
    short_description = db.Column(db.String(320), default='')
    long_description = db.Column(db.String(2000), default='')
    publisher_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    job_reference_number = db.Column(db.String(300))
    job_title = db.Column(db.String(300))
    type_of_contract = db.Column(db.String(300))
    duration = db.Column(db.String(300))
    availability_from = db.Column(db.DateTime(), index=True)
    availability_to = db.Column(db.DateTime(), index=True)
    positions = db.Column(db.String(200))
    additional_info = db.Column(db.String(2000), default='')
    policy = db.Column(db.String(2000), default='')
    published = db.Column(db.Boolean(), index=True, default=False)
    requires_license = db.Column(db.Boolean(), default=False)
    # Relationships
    applications = db.relationship('Application', backref='listing', lazy='dynamic')
    images = db.relationship('Image', backref='listing', lazy='dynamic')
    categories = db.relationship('Category', secondary=listing_tags, backref='listing', lazy='dynamic')

    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Listing\'%s\'>' % self.job_title

    def get_applications_count(self):
        count = Application.query.filter_by(job_listing_id=self.id).count()
        return count


def get_search(name, date, page):
    if name != '':
        first = Listing.query.filter(db.false())
        if list:
            first = Listing.query.filter(Listing.id.in_(list)).filter(Listing.availability_to > date).filter(
                Listing.status == True)
        own = Listing.query.filter(
            and_(Listing.location.like('%' + name.capitalize() + '%'), Listing.availability_to > date)).filter(
            Listing.status == True)
        listings = first.union(own).order_by(Listing.rating.desc()).paginate(page,
                                                                             current_app.config['POSTS_PER_PAGE'],
                                                                             False)
    else:
        listings = Listing.query.filter(Listing.availability_to > date).filter(Listing.status == True).order_by(
            Listing.rating.desc()).paginate(
            page, current_app.config['POSTS_PER_PAGE'], False)

    return listings
