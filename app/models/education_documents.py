from datetime import datetime

from .. import db


class EducationDocuments(db.Model):
    __tablename__ = 'education_documents'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    cv_url = db.Column(db.String(256))
    cover_letter_url = db.Column(db.String(256))
    application_form_url = db.Column(db.String(256))
    license = db.Column(db.String(256))
    years_of_experience = db.Column(db.Integer)
    application_id = db.Column(db.Integer, db.ForeignKey('applications.id'))
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<EducationDocuments\'%s\'>' % self.id


