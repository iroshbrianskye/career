from datetime import datetime

from .. import db


class EmploymentHistory(db.Model):
    __tablename__ = 'employment_history'
    id = db.Column(db.Integer, primary_key=True)
    name_of_institution = db.Column(db.String(64), index=True)
    year_from = db.Column(db.String(64), index=True)
    year_to = db.Column(db.String(64))
    duration = db.Column(db.String(64), index=True)
    job_group = db.Column(db.String(64), index=True)
    designation = db.Column(db.String(64))
    gross_salary = db.Column(db.String(64))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'), index=True)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<EmploymentHistory \'%s\'>' % self.id


class Employment(db.Model):
    __tablename__ = 'employment'
    id = db.Column(db.Integer, primary_key=True)
    ministry = db.Column(db.String(64), index=True)
    station = db.Column(db.String(64), index=True)
    personal_employment_no = db.Column(db.String(64), index=True)
    present_substantive_post = db.Column(db.String(64), index=True)
    job_group = db.Column(db.String(64), index=True)
    date_of_current_appointment = db.Column(db.DateTime())
    effective_date = db.Column(db.DateTime())
    position_held = db.Column(db.String(64))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'), index=True)
    terms_of_service = db.Column(db.String(64), index=True)
    if_other = db.Column(db.String(200))
    gross_salary = db.Column(db.String(64))
    current_employer = db.Column(db.String(64))

    def __repr__(self):
        return '<Employment \'%s\'>' % self.id
