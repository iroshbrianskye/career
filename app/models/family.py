from datetime import datetime

from .. import db


class Family(db.Model):
    __tablename__ = 'family'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    relationship = db.Column(db.String(64), index=True)
    name = db.Column(db.String(64), index=True)
    phone_number = db.Column(db.String(64), index=True)
    gender = db.Column(db.String(64), index=True)
    dob = db.Column(db.DateTime())
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Family\'%s\'>' % self.name


