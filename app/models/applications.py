from datetime import datetime

from .. import db
from flask_moment import Moment


class Application(db.Model):
    __tablename__ = 'applications'
    id = db.Column(db.Integer, primary_key=True)
    job_listing_id = db.Column(db.Integer, db.ForeignKey('job_listings.id'))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    complete_profile = db.Column(db.Boolean(), index=True, default=False)
    agreement = db.Column(db.Boolean(), index=True, default=False)
    status = db.Column(db.String(120), default='pending')
    education_documents = db.relationship('EducationDocuments', backref='applications', lazy='dynamic')
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Application\'%s\'>' % self.job_listing_id

    def get_time(self):
        return self.createdAt
