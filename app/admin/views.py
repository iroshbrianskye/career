import os

from pandas.io.sql import execute
from sqlalchemy import create_engine, String
import pyexcel_xls
import pyexcel_xlsx

import app
import pandas as pd
from config import Config, DevelopmentConfig
from app import excel
from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    send_from_directory,
    current_app,
    jsonify,
    send_file)

from app.models import *
from app.decorators import admin_required
from flask_login import current_user, login_required
from app.admin.forms import *
from app.auth.forms import *
from flask_rq import get_queue
from app.email import send_email
from app.auth.email import send_confirm_email
from app.auth.email import send_password_reset_email
from app.auth.admin_decorators import check_confirmed
from sqlalchemy import func
from flask_ckeditor import upload_success, upload_fail

admin = Blueprint("admin", __name__)
photos = UploadSet("photos", IMAGES)

db_URI = DevelopmentConfig.SQLALCHEMY_DATABASE_URI
engine = create_engine(db_URI)


@admin.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@admin.route("/")
@login_required
@admin_required
@check_confirmed
def dashboard():
    """Admin dashboard page."""
    all_applications = Application.query.filter_by(status='applied').order_by(Application.createdAt.desc()).all()
    published_listings = (
        Listing.query.filter_by(published=True)
            .order_by(Listing.createdAt.desc())
            .all()
    )
    all_listings = Listing.query.order_by(Listing.createdAt.desc()).all()
    latest_listings = Listing.query.order_by(Listing.createdAt.desc()).limit(4)
    croles = Role.query.filter_by(index="applicant").first_or_404()
    applicants = croles.users.order_by(User.createdAt.desc()).limit(5)
    publishers = Publisher.query.order_by(Publisher.createdAt.desc()).limit(5)
    interactionsCount = Application.query.count()
    applicationsCount = Application.query.filter_by(status="applied").count()
    listingsCount = Listing.query.filter_by(published=True).count()
    publishersCount = publishers.count()
    applicantsCount = applicants.count()
    # titles= [listing.title for listing in listings]

    # for listing in listings:
    #     count = Application.query.filter_by(job_listing_id=listing.id).count()
    #     list.append(count)
    return render_template(
        "admin/index.html",
        all_applications=all_applications,
        published_listings=published_listings,
        applicationsCount=applicationsCount,
        listingsCount=listingsCount,
        publishersCount=publishersCount,
        applicantsCount=applicantsCount,
        publishers=publishers,
        applicants=applicants,
        all_listings=all_listings,
        latest_listings=latest_listings,
        interactionsCount=interactionsCount
    )


@admin.route("/all_publishers")
@login_required
@admin_required
@check_confirmed
def publishers():
    """Publisher dashboard page."""
    publishers = Publisher.query.order_by(Publisher.createdAt.desc()).all()
    return render_template("admin/publishers.html", publishers=publishers)


@admin.route("/all_applicants")
@login_required
@admin_required
def applicants():
    """Admin dashboard page."""
    applicants = Applicant.query.order_by(Applicant.createdAt.desc()).all()
    return render_template("admin/applicants.html", applicants=applicants)


@admin.route("/all_applications")
@login_required
@admin_required
@check_confirmed
def applications():
    """Admin dashboard page."""
    applications = Application.query.filter_by(status="applied").order_by(Application.createdAt.desc()).all()
    family = Family
    education_documents = EducationDocuments
    education_details = Education
    p = Professional
    m = Membership
    employment = EmploymentHistory
    empL = EmploymentHistory.query.order_by(EmploymentHistory.createdAt.desc())
    return render_template("admin/applications.html", applications=applications,
                           # education_details=education_details,
                           # employment_details=employment_details,
                           # employment_history=employment_history,
                           # family=family,
                           # referees=referees,
                           p=p,
                           m=m,
                           family=family,
                           education_details=education_details,
                           education_documents=education_documents,
                           employment=employment,
                           empL=empL
                           )


@admin.route("/confirm_application/<id>/<status>")
@login_required
@admin_required
@check_confirmed
def confirm_application(id, status):
    application = Application.query.filter_by(id=id).first_or_404()
    if application.listing.publisher != current_user:
        return redirect(url_for("home.index"))
    application.status = status
    db.session.commit()
    flash("Status Updated", "green")
    return redirect(url_for("publisher.applications"))


@admin.route("/cancel_application", methods=["POST"])
@login_required
@admin_required
@check_confirmed
def cancel_application():
    id = request.args.get("id", 0, type=int)
    if request.method == "POST":
        application = Application.query.filter_by(id=id).first_or_404()
        if application.listing.publisher != current_user:
            return 401
        application.status = "cancelled"
        application.reason = request.form.get("reason")
        db.session.commit()
    applications = (
        Application.query.join(Listing, (Listing.id == Application.job_listing_id))
            .filter(Listing.publisher_id == current_user.id)
            .order_by(Application.createdAt.desc())
            .all()
    )
    return render_template("admin/_applications.html", applications=applications)


@admin.route("/all_listings")
@login_required
@admin_required
def job_listings():
    """All Listings Page."""
    # page = request.args.get("page", 1, type=int)
    listings = (
        Listing.query.order_by(Listing.createdAt.desc()).all()
    )

    return render_template("admin/listings.html", listings=listings)


@admin.route("/view_listing/<id>")
@login_required
@admin_required
@check_confirmed
def job_listing_id(id):
    """Single listing page."""
    listing = Listing.query.filter_by(id=id).first_or_404()
    job_applications = Application.query.filter_by(job_listing_id=listing.id).all()
    family = Family
    education_documents = EducationDocuments
    education_details = Education
    p = Professional
    m = Membership
    employment = EmploymentHistory
    return render_template("admin/view_listing.html",
                           listing=listing, job_applications=job_applications,
                           family=family, education_documents=education_documents,
                           education_details=education_details, p=p, m=m, employment=employment)


@admin.route("/listing_details")
@login_required
@admin_required
@check_confirmed
def job_listing_details():
    """All Listings Page."""
    id = request.args.get("since", 0, type=int)
    listing = Listing.query.filter_by(id=id).first_or_404()
    return render_template("admin/_listing_details.html", listing=listing)


@admin.route("/create_job_listing", methods=("GET", "POST"))
@login_required
@admin_required
@check_confirmed
def newListing():
    form = ListingForm()
    if form.validate_on_submit():
        new_job_listing = Listing(
            short_description=form.short_description.data,
            long_description=form.long_description.data,
            availability_from=form.availability_from.data,
            availability_to=form.availability_to.data,
            job_reference_number=form.job_reference_number.data,
            job_title=form.job_title.data,
            positions=form.positions.data,
            additional_info=form.additional_info.data,
            policy=form.policy.data,
            publisher=current_user,
            requires_license=form.requires_license.data
        )
        db.session.add(new_job_listing)
        db.session.commit()
        return redirect(url_for("admin.job_listings"))
    return render_template("admin/add_listing.html", form=form)


@admin.route("/edit_job_listing/<id>", methods=("GET", "POST"))
@login_required
@admin_required
@check_confirmed
def edit_listing(id):
    """Edit Job Listing page."""
    listing = Listing.query.filter_by(id=id).first_or_404()
    form = EditListingForm(obj=listing)

    if form.validate_on_submit():
        form.populate_obj(listing)
        db.session.commit()
        return redirect(url_for('admin.job_listings'))
    return render_template('admin/edit_listing.html', form=form, listing=listing)


@admin.route("/listings/delete/<id>")
@login_required
@admin_required
@check_confirmed
def delete_listing(id):
    listing = Listing.query.filter_by(id=id).first_or_404()
    db.session.delete(listing)
    db.session.commit()
    flash("Listing Deleted successfully", "success")
    return redirect(url_for("admin.job_listings"))


@admin.route("/view_publisher/<user_id>")
@login_required
@admin_required
@check_confirmed
def publisher_by(user_id):
    """Publisher dashboard page."""
    roles = Role.query.filter_by(index="publisher").first_or_404()
    publishers = roles.users
    return render_template("admin/manage_user.html", publishers=publishers)


@admin.route("/view_applicant/<id>")
@login_required
@admin_required
@check_confirmed
def view_applicant(id):
    """Admin dashboard page."""
    return render_template("admin/index.html")


@admin.route("/view_application/<id>")
@login_required
@admin_required
@check_confirmed
def view_application(id):
    """View application page."""
    application = Application.query.filter_by(id=id).first_or_404()
    user_id = application.user_id
    education_details = Education.query.filter_by(user_id=user_id).all()
    education_documents = EducationDocuments.query.filter_by(user_id=user_id).filter_by(
        application_id=application.id).first()
    employment_details = Employment.query.filter_by(user_id=user_id).first()
    employment_history = EmploymentHistory.query.filter_by(user_id=user_id).all()
    family = Family.query.filter_by(user_id=user_id).first_or_404()
    referees = Reference.query.filter_by(user_id=user_id).first_or_404()
    prof = Professional.query.filter_by(user_id=user_id).all()
    job_applied = Listing.query.filter_by(id=application.job_listing_id).first()
    return render_template("admin/view_application.html", application=application,
                           education_details=education_details, employment_details=employment_details,
                           employment_history=employment_history, family=family, referees=referees,
                           education_documents=education_documents, prof=prof, job_applied=job_applied)


@admin.route("/export_applications", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def download_applications_xls():
    query_sets = User.query.join(Application, Application.user_id == User.id) \
        .order_by(Application.createdAt.desc()).all()
    column_names = ['id', 'first_name']

    return excel.make_response_from_a_table(db.session, Application, file_type="xls",
                                            file_name='applications.xls')


@admin.route("/add_new_user", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def new_user():
    """Create a new user."""
    form = NewUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            email=form.email.data,
            password=form.password.data,
            comfirmed='True'
        )
        db.session.add(user)
        db.session.commit()
        flash("User {} successfully created".format(user.full_name()), "success")
    return render_template("admin/new_user.html", form=form)


@admin.route("/invite-user", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def invite_user():
    """Invites a new user to create an account and set their own password."""
    form = InviteUserForm()
    if form.validate_on_submit():
        user = User(
            role=form.role.data,
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            email=form.email.data,
        )
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        invite_link = url_for(
            "account.join_from_invite", user_id=user.id, token=token, _external=True
        )
        get_queue().enqueue(
            send_email,
            recipient=user.email,
            subject="You Are Invited To Join",
            template="account/email/invite",
            user=user,
            invite_link=invite_link,
        )
        flash("User {} successfully invited".format(user.full_name()), "form-success")
    return render_template("admin/invite_user.html", form=form)


@admin.route("/all_users")
@login_required
@admin_required
@check_confirmed
def registered_users():
    """View all registered users."""
    users = User.query.order_by(User.createdAt.desc()).all()
    roles = Role.query.all()
    return render_template("admin/registered_users.html", users=users, roles=roles)


@admin.route("/view_user/<int:user_id>")
@admin.route("/view_user/<int:user_id>/info")
@login_required
@admin_required
@check_confirmed
def user_info(user_id):
    """View a user's profile."""
    user = User.query.filter_by(id=user_id).first()
    applications = Application.query.filter_by(user_id=user_id).all()
    applicationsCount = Application.query.filter_by(user_id=user_id).count()
    listingsCount = Listing.query.filter_by(publisher_id=user_id).count()
    if user is None:
        abort(404)
    return render_template(
        "admin/manage_user.html",
        user=user,
        applications=applications,
        applicationsCount=applicationsCount,
        listingsCount=listingsCount,
    )


@admin.route("/user/<int:user_id>/change-email", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_user_email(user_id):
    """Change a user's email."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    form = ChangeUserEmailForm()
    if form.validate_on_submit():
        user.email = form.email.data
        db.session.add(user)
        db.session.commit()
        flash(
            "Email for user {} successfully changed to {}.".format(
                user.full_name(), user.email
            ),
            "form-success",
        )
    return render_template("admin/manage_user.html", user=user, form=form)


@admin.route("/user/<int:user_id>/change-account-type", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_account_type(user_id):
    """Change a user's account type."""
    if current_user.id == user_id:
        flash(
            "You cannot change the type of your own account. Please ask "
            "another administrator to do this.",
            "error",
        )
        return redirect(url_for("admin.user_info", user_id=user_id))

    user = User.query.get(user_id)
    if user is None:
        abort(404)
    form = ChangeAccountTypeForm()
    if form.validate_on_submit():
        user.role = form.role.data
        db.session.add(user)
        db.session.commit()
        flash(
            "Role for user {} successfully changed to {}.".format(
                user.full_name(), user.role.name
            ),
            "form-success",
        )
    return render_template("admin/manage_user.html", user=user, form=form)


@admin.route("/user/<int:user_id>/delete")
@login_required
@admin_required
@check_confirmed
def delete_user_request(user_id):
    """Request deletion of a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template("admin/manage_user.html", user=user)


@admin.route("/user/<int:user_id>/_delete")
@login_required
@admin_required
def delete_user(user_id):
    """Delete a user's account."""
    if current_user.id == user_id:
        flash(
            "You cannot delete your own account. Please ask another "
            "administrator to do this.",
            "error",
        )
    else:
        user = User.query.filter_by(id=user_id).first()
        db.session.delete(user)
        db.session.commit()
        flash("Successfully deleted user %s." % user.full_name(), "success")
    return redirect(url_for("admin.registered_users"))


@admin.route("/approve_job_listing/<job_listing_id>")
@login_required
@admin_required
@check_confirmed
def publish_listing(job_listing_id):
    listing = Listing.query.filter_by(id=job_listing_id).first_or_404()
    if listing.published:
        listing.published = False
    else:
        listing.published = True
    db.session.commit()
    flash("Successfully changed Listing approval for publishing", "success")
    return redirect(url_for("admin.job_listings"))


@admin.route("/<int:user_id>/_suspend/<sender>")
@login_required
@admin_required
@check_confirmed
def suspend(user_id, sender):
    """Suspend a user's account."""
    user = User.query.filter_by(id=user_id).first()
    if current_user.id == user_id:
        flash(
            "You cannot suspend your own account. Please ask another "
            "administrator to do this.",
            "error",
        )
    else:
        if user.status == 1:
            user.status = 0
            if user.role.index == "publisher":
                for listing in user.listings:
                    listing.status = 0
                    listing.published = 0
        else:
            user.status = 1
        db.session.commit()
        flash("Successfully suspended user %s." % user.full_name(), "success")
    if sender == "publisher":
        return redirect(url_for("admin.publishers"))
    else:
        return redirect(url_for("admin.applicants"))


@admin.route("/disclaimer", methods=("GET", "POST"))
@login_required
@admin_required
@check_confirmed
def disclaimer():
    disclaimer = Disclaimer.query.first()
    form = DisclaimerForm(obj=disclaimer)
    if form.validate_on_submit():
        if disclaimer:
            form.populate_obj(disclaimer)
        else:
            disclaimer = Disclaimer(text=form.text.data)
            db.session.add(disclaimer)
        db.session.commit()
        flash("Disclaimer edited successfully", "success")
        return redirect(url_for("admin.disclaimer"))
    return render_template("admin/disclaimer.html", form=form)


@admin.route("/settings", methods=("GET", "POST"))
@login_required
@admin_required
def admin_settings():
    user = current_user

    return render_template("admin/settings.html", user=user)


@admin.route("/settings/change_password", methods=("GET", "POST"))
@login_required
@admin_required
@check_confirmed
def change_password():
    """Change an existing user's password."""
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.new_password.data
            db.session.add(current_user)
            db.session.commit()
            flash("Your password has been updated.", "success")
            return redirect(url_for("admin.settings"))
        else:
            flash("Original password is invalid.", "red")
    return render_template("admin/change_password.html", form=form)


@admin.route("/settings/change-email", methods=["GET", "POST"])
@admin_required
@login_required
@check_confirmed
def change_email_request():
    """Respond to existing user's request to change their email."""
    form = ChangeEmailForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.password.data):
            new_email = form.email.data
            token = current_user.generate_email_change_token(new_email)
            change_email_link = url_for(
                "account.change_email", token=token, _external=True
            )
            get_queue().enqueue(
                send_email,
                recipient=new_email,
                subject="Confirm Your New Email",
                template="account/email/change_email",
                # current_user is a LocalProxy, we want the underlying user
                # object
                user=current_user._get_current_object(),
                change_email_link=change_email_link,
            )
            flash(
                "A confirmation link has been sent to {}.".format(new_email), "warning"
            )
            return redirect(url_for("admin.settings"))
        else:
            flash("Invalid email or password.", "form-error")
    return render_template("admin/change_email.html", form=form)


@admin.route("/settings/change-email/<token>", methods=["GET", "POST"])
@login_required
@admin_required
@check_confirmed
def change_email(token):
    """Change existing user's email with provided token."""
    if current_user.change_email(token):
        flash("Your email address has been updated.", "success")
    else:
        flash("The confirmation link is invalid or has expired.", "error")
    return redirect(url_for("admin.dashboard"))


@admin.route("/category")
@admin_required
@check_confirmed
def category():
    categories = Category.query.order_by(Category.createdAt.desc()).all()
    return render_template("admin/category.html", items=categories)


@admin.route("/category/new", methods=("GET", "POST"))
@admin_required
@check_confirmed
def newCategory():
    form = CategoryForm()
    if form.validate_on_submit():
        image = form.image.data
        if image:
            image = photos.save(form.image.data)
        category = Category(name=form.name.data, image_url=image)
        db.session.add(category)
        db.session.commit()
        flash("Category added successfully", "success")
        return redirect(url_for("admin.category"))
    return render_template("admin/add_category.html", form=form)


@admin.route("/files/<path:filename>")
def uploaded_files(filename):
    path = current_app.config["UPLOADS_CKEDITOR"]
    return send_from_directory(path, filename)


@admin.route("/upload", methods=["POST"])
def upload():
    f = request.files.get("upload")
    # Add more validations here
    extension = f.filename.split(".")[1].lower()
    if extension not in ["jpg", "gif", "png", "jpeg"]:
        return upload_fail(message="Image only!")
    f.save(os.path.join("app/static/uploads", f.filename))
    url = url_for("admin.uploaded_files", filename=f.filename)
    return upload_success(url=url)  # return upload_success call


# serve static files
@admin.route("/thumbnail/<string:filename>", methods=["GET"])
def get_thumbnail(filename):
    return send_from_directory(
        current_app.config["THUMBNAIL_FOLDER"], filename=filename
    )


@admin.route("/data/<string:filename>", methods=["GET"])
def get_file(filename):
    return send_from_directory(
        os.path.join(current_app.config["UPLOAD_FOLDER"]), filename=filename
    )
