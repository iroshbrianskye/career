from flask_wtf import Form, FlaskForm
from wtforms import ValidationError
from flask_ckeditor import CKEditorField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.fields import (
    PasswordField,
    StringField,
    SubmitField,
    SelectField,
    TextAreaField,
    FileField,
    DateField,
    IntegerField,
    BooleanField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_wtf.file import FileField, FileRequired, FileAllowed
from app import db
from app.models import Role, User, Category

photos = UploadSet("photos", IMAGES)


def get_pk(obj):
    return str(obj)


class ChangeUserEmailForm(FlaskForm):
    email = EmailField(
        "New email", validators=[InputRequired(), Length(1, 64), Email()]
    )
    submit = SubmitField("Update email")

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email already registered.")


class ChangeAccountTypeForm(FlaskForm):
    role = QuerySelectField(
        'New account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'), get_pk=get_pk)
    submit = SubmitField('Update role')


class InviteUserForm(FlaskForm):
    role = QuerySelectField(
        'Account type',
        validators=[InputRequired()],
        get_label='name',
        query_factory=lambda: db.session.query(Role).order_by('permissions'), get_pk=get_pk)
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    second_name = StringField("Last name", validators=[InputRequired(), Length(1, 64)])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    submit = SubmitField("Invite")

    def validate_email(self, field):
        if User.query.filter_by(email=field.data).first():
            raise ValidationError("Email already registered.")


class NewUserForm(InviteUserForm):
    password = PasswordField("Password", validators=[InputRequired()])
    password2 = PasswordField(
        "Confirm password",
        validators=[InputRequired(), EqualTo("password", "Passwords must match.")],
    )

    submit = SubmitField("Create")


class ListingForm(FlaskForm):
    short_description = StringField("Short Description")
    long_description = StringField("Long Description")
    job_reference_number = StringField("Job Reference Number", validators=[InputRequired()])
    job_title = StringField("Job Title", validators=[InputRequired()])
    availability_from = DateField('Availability From', validators=[DataRequired()], format='%d/%m/%Y')
    availability_to = DateField("Availability To", format="%d/%m/%Y", validators=[DataRequired()])
    positions = StringField("Available Positions", validators=[DataRequired()])
    additional_info = TextAreaField("Additional Info")
    policy = TextAreaField("Policy")
    requires_license = BooleanField(default=False)

    submit = SubmitField("Create Job Listing")


class EditListingForm(FlaskForm):
    title = StringField("Title")
    short_description = StringField("Short Description")
    long_description = StringField("Long Description")
    job_reference_number = StringField("Job Reference Number", validators=[InputRequired()])
    job_title = StringField("Job Title", validators=[InputRequired()])
    availability_from = DateField("Availability From", format="%d/%m/%Y", validators=[DataRequired()])
    availability_to = DateField("Availability To", format="%d/%m/%Y", validators=[DataRequired()])
    positions = IntegerField("Available Positions", validators=[InputRequired()])
    additional_info = TextAreaField("Additional Info")
    policy = TextAreaField("Policy")

    submit = SubmitField("Edit Job Listing")


class CategoryForm(FlaskForm):
    name = StringField("name", validators=[Length(min=2, max=80)])
    image = FileField(validators=[FileAllowed(photos, u"Image only!")])
    submit = SubmitField("save")

    def validate_name(self, name):
        category = Category.query.filter_by(name=name.data).first()
        if category is not None:
            raise ValidationError("This category has already been added")


class DisclaimerForm(FlaskForm):
    text = StringField("Text")
    submit = SubmitField("Save")



